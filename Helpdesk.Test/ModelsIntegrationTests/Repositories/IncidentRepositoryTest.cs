﻿using System.Data.Entity.Validation;
using System.Linq;
using Helpdesk.Models.Enums;
using Helpdesk.Models.Repositories;
using Helpdesk.Models.Requests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Helpdesk.Test.ModelsIntegrationTests.Repositories
{
    [TestClass]
    public class IncidentRepositoryTest
    {
        [TestMethod]
        public void ShouldGetIncidentsFromDatabase()
        {
            var repository = new IncidentRepository();
            var incidents = repository.GetAll();
            var incidentsNumber = incidents.Count();
            Assert.IsTrue(incidentsNumber >= 0);
        }

        [TestMethod]
        public void ShouldAddIncidentToDatabase()
        {
            var insertedIncident = InsertIncident();
            var repository = new IncidentRepository();

            Assert.IsNotNull(repository.Get(insertedIncident.Id));

            repository.Delete(insertedIncident);

            Assert.IsNull(repository.Get(insertedIncident.Id));
        }


        [TestMethod]
        [ExpectedException(typeof(DbEntityValidationException))]
        public void ShouldThrowExceptionWhenModelHasInvalidData()
        {
            var repository = new IncidentRepository();
            repository.Add(new Incident
            {
                Category = IncidentCategory.BrokenConnection,
                Description = "a",
                Title = "a"
            });
        }

        [TestMethod]
        public void ShouldDeleteIncidentFromDatabase()
        {
            var incident = InsertIncident();
            var repository = new IncidentRepository();
            var deletedIncidentId = incident.Id;
            repository.Delete(incident);

            Assert.AreEqual(null, repository.Get(deletedIncidentId));
        }

        [TestMethod]
        public void ShouldUpdateIncident()
        {
            var repository = new IncidentRepository();

            var title = "aaaa";
            var description = "bbbb";
            var category = IncidentCategory.GithubEvent;
            var requestor = "requestorName";
            var incident = new Incident {Id=1, Title = title, Description = description, Category = category, Requestor = requestor };
            repository.Update(incident);


            Assert.AreEqual(title, repository.Get(1).Title);
            Assert.AreEqual(description, repository.Get(1).Description);
            Assert.AreEqual(category, repository.Get(1).Category);
            Assert.AreEqual(requestor, repository.Get(1).Requestor);

            incident = new Incident { Id = 1, Title = "zzz", Description = "zzz", Category = IncidentCategory.BrokenConnection, Requestor = "aaa" };
            repository.Update(incident);
        }

        private Incident InsertIncident()
        {
            var repository = new IncidentRepository();
            var insertedIncident = repository.Add(new Incident
            {
                Category = IncidentCategory.BrokenConnection,
                Description = "description",
                Requestor = "darek.kacban@gmail.com",
                Title = "title"
            });

            return insertedIncident;
        }
    }
}
