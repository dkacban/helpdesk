namespace Helpdesk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Incidents", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Incidents", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Incidents", "Requestor", c => c.String(nullable: false));
            AlterColumn("dbo.ServiceRequests", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.ServiceRequests", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.ServiceRequests", "Requestor", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ServiceRequests", "Requestor", c => c.String());
            AlterColumn("dbo.ServiceRequests", "Description", c => c.String());
            AlterColumn("dbo.ServiceRequests", "Title", c => c.String());
            AlterColumn("dbo.Incidents", "Requestor", c => c.String());
            AlterColumn("dbo.Incidents", "Description", c => c.String());
            AlterColumn("dbo.Incidents", "Title", c => c.String());
        }
    }
}
