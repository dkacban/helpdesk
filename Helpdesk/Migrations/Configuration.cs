namespace Helpdesk.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Helpdesk.Models.HelpdeskDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Helpdesk.Models.HelpdeskDbContext context)
        {
        }
    }
}
