namespace Helpdesk.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Journals", "Incident_Id", c => c.Int());
            AddColumn("dbo.Journals", "ServiceRequest_Id", c => c.Int());
            CreateIndex("dbo.Journals", "Incident_Id");
            CreateIndex("dbo.Journals", "ServiceRequest_Id");
            AddForeignKey("dbo.Journals", "Incident_Id", "dbo.Incidents", "Id");
            AddForeignKey("dbo.Journals", "ServiceRequest_Id", "dbo.ServiceRequests", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Journals", "ServiceRequest_Id", "dbo.ServiceRequests");
            DropForeignKey("dbo.Journals", "Incident_Id", "dbo.Incidents");
            DropIndex("dbo.Journals", new[] { "ServiceRequest_Id" });
            DropIndex("dbo.Journals", new[] { "Incident_Id" });
            DropColumn("dbo.Journals", "ServiceRequest_Id");
            DropColumn("dbo.Journals", "Incident_Id");
        }
    }
}
