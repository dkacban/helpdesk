﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Helpdesk.Models;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin.Security;

namespace Helpdesk
{
    public class ClaimsHelper
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string clientId = ConfigurationManager.AppSettings["ida:ClientId"];
        private string appKey = ConfigurationManager.AppSettings["ida:ClientSecret"];
        private string aadInstance = ConfigurationManager.AppSettings["ida:AADInstance"];
        private string graphResourceID = "https://graph.windows.net";
        private IUser _user;
        private ActiveDirectoryClient _activeDirectoryClient;
        private IEnumerable<string> _curentUserGrtoupsIds;
        private HttpContext _context;
        private string _tenantId;

        private async Task InitUser()
        {
            _tenantId = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;
            string userObjectId = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;
            Uri servicePointUri = new Uri(graphResourceID);
            Uri serviceRoot = new Uri(servicePointUri, _tenantId);
            _activeDirectoryClient = new ActiveDirectoryClient(serviceRoot,
                async () => await GetTokenForApplication());

            var result = await _activeDirectoryClient.Users
                .Where(u => u.ObjectId.Equals(userObjectId))
                .ExecuteAsync();
            _user = result.CurrentPage.ToList().First();
        }

        private async Task InitGroups()
        {
            var currentUserGroupIds = await _user.GetMemberGroupsAsync(false);
            _curentUserGrtoupsIds = currentUserGroupIds.ToList();
        }

        public ClaimsHelper(HttpContext context)
        {
            _context = context;
            Task.Run(() => InitUser()).Wait();
            Task.Run(() => InitGroups()).Wait();
        }

        private async Task<string> GetTokenForApplication()
        {
            string signedInUserId = ClaimsPrincipal.Current.FindFirst(ClaimTypes.NameIdentifier).Value;
            string userObjectId = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value;

            ClientCredential clientcred = new ClientCredential(clientId, appKey);
            AuthenticationContext authenticationContext = new AuthenticationContext(aadInstance + _tenantId, new ADALTokenCache(signedInUserId));
            AuthenticationResult authenticationResult = await authenticationContext.AcquireTokenSilentAsync(graphResourceID, clientcred, new UserIdentifier(userObjectId, UserIdentifierType.UniqueId));
            return authenticationResult.AccessToken;
        }
        public async Task UpdateHelpDeskClaim()
        {
            var identity = ClaimsPrincipal.Current.Identity as ClaimsIdentity;
            var helpdeskClaim = identity.FindFirst("Helpdesk");
            if (helpdeskClaim != null)
            {
                identity.RemoveClaim(helpdeskClaim);
            }

            var groups = await _activeDirectoryClient.Groups.ExecuteAsync();
            var groupNames =
                groups.CurrentPage.ToList().Where(g => _curentUserGrtoupsIds.Contains(g.ObjectId)).Select(g => g.DisplayName);
            var groupNamesList = groupNames.ToList();

            var isHelpdesk = "false";
            if (groupNamesList.Contains("Helpdesk"))
            {
                isHelpdesk = "true";
            }

            var newHelpdeskClaim = new Claim("Helpdesk", isHelpdesk);
            identity.AddClaim(newHelpdeskClaim);
            var authenticationManager = _context.GetOwinContext().Authentication;
            authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties() { IsPersistent = true });
        }

        public IUser GetUser()
        {
            return _user;
        }

        public bool IsHelpdeskClaimSet()
        {
            var identity = ClaimsPrincipal.Current.Identity as ClaimsIdentity;
            bool isHepldesk = false;
            if (identity != null)
            {
                var value = identity.Claims.FirstOrDefault(c => c.Type == "Helpdesk")?.Value;
                bool result;
                var success = bool.TryParse(value, out result);
                isHepldesk = success && result;
            }

            return isHepldesk;
        }

        public async Task<IList<string>> GetGroupNames()
        {
            var groups = await _activeDirectoryClient.Groups.ExecuteAsync();
            var groupNames =
                groups.CurrentPage.ToList().Where(g => _curentUserGrtoupsIds.Contains(g.ObjectId)).Select(g => g.DisplayName);

            return groupNames.ToList();
        }
    }
}