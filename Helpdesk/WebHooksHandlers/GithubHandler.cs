﻿using System.Linq;
using System.Threading.Tasks;
using Helpdesk.Models.Enums;
using Helpdesk.Models.Repositories;
using Helpdesk.Models.Requests;
using Microsoft.AspNet.WebHooks;
using Newtonsoft.Json.Linq;

namespace Helpdesk.WebHooksHandlers
{

    //This handles all events that happen in the project: https://github.com/dkacban/helpdesk
    public class GitHubHandler : WebHookHandler
    {
        public override Task ExecuteAsync(string receiver, WebHookHandlerContext context)
        {
            string action = context.Actions.First();
            JObject data = context.GetDataOrDefault<JObject>();
            var gitHubUserName = data["issue"]["user"]["login"].Value<string>();
            var repo = new IncidentRepository();
            repo.Add(new Incident {Category = IncidentCategory.GithubEvent, Title = action, Description = data.ToString(), Requestor = gitHubUserName });

            return Task.FromResult(true);
        }
    }
}