﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Helpdesk.Models;
using Helpdesk.Models.Repositories;
using Helpdesk.Models.Requests;

namespace Helpdesk.Controllers
{
    [Authorize]
    public class IncidentController : Controller
    {
        private readonly IIncidentRepository _incidentRepository;
        private readonly ClaimsHelper _claimsHelper;

        public IncidentController(IIncidentRepository incidentRepository, ClaimsHelper claimsHelper)
        {
            _incidentRepository = incidentRepository;
            _claimsHelper = claimsHelper;
        }

        public async Task<ActionResult> Index()
        {
            await _claimsHelper.UpdateHelpDeskClaim();
            var isHelpdesk = _claimsHelper.IsHelpdeskClaimSet();
            ViewBag.helpdesk = isHelpdesk;
            var incidents = isHelpdesk ?
                _incidentRepository.GetAll() :
                _incidentRepository.GetAll().Where(i => i.Requestor == User.Identity.Name);

            return View(incidents);
        }

        public ActionResult Create()
        {
            var incident = new Incident();
            return View(incident);
        }
        public ActionResult Edit(int id)
        {
            var repo = new IncidentRepository();
            var incident = repo.Get(id);

            var isHelpdesk = _claimsHelper.IsHelpdeskClaimSet();
            if (incident.Requestor == User.Identity.Name || isHelpdesk)
            {
                return View(incident);
            }

            return RedirectToAction("Index", "Incident");
        }

        [HttpPost]
        public ActionResult Edit(Incident incident)
        {
            if (ModelState.IsValid)
            {
                var repo = new IncidentRepository();
                repo.Update(incident);

                return Redirect("/incident");
            }

            return View();
        }

        [HttpPost]
        public ActionResult Create(Incident incident)
        {
            var repo = new IncidentRepository();
            incident.Requestor = User.Identity.Name;
            repo.Add(incident);

            return Redirect("/incident");
        }
    }
}