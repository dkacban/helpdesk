﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;

namespace Helpdesk.Controllers
{
    [Authorize]
    public class UserProfileController : Controller
    {
        private readonly ClaimsHelper _claimsHelper;
        public UserProfileController(ClaimsHelper claimsHelper)
        {
            _claimsHelper = claimsHelper;
        }

        public async Task<ActionResult> Index()
        {
            try
            {
                var helper = _claimsHelper;
                var groupNames = await helper.GetGroupNames();
                ViewBag.groups = string.Join(",", groupNames);
                var user = helper.GetUser();

                return View(user);
            }
            catch (AdalException)
            {
                return View("Error");
            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message;

                return View("Relogin");
            }
        }

        public void RefreshSession()
        {
            HttpContext.GetOwinContext().Authentication.Challenge(
                new AuthenticationProperties { RedirectUri = "/UserProfile" },
                OpenIdConnectAuthenticationDefaults.AuthenticationType);
        }
    }
}
