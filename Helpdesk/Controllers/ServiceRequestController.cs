﻿using System.Web.Mvc;

namespace Helpdesk.Controllers
{
    [Authorize]
    public class ServiceRequestController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
    }
}