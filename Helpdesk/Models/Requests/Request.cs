﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Helpdesk.Models.Requests
{
    public class Request
    {
        public int Id { get; set; }
        [Required]
        [MinLength(3)]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Requestor { get; set; }
        public virtual ICollection<Journal> Journals { get; set; }
    }
}