﻿using Helpdesk.Models.Enums;

namespace Helpdesk.Models.Requests
{
    public class ServiceRequest : Request
    {
        public ServiceRequestCategory Category { get; set; }
    }
}