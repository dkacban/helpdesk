﻿using System.ComponentModel.DataAnnotations;
using Helpdesk.Models.Enums;

namespace Helpdesk.Models.Requests
{
    public class Incident : Request
    {
        [Required]
        public IncidentCategory Category { get; set; }
    }
}