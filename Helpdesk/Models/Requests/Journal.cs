﻿using System;

namespace Helpdesk.Models.Requests
{
    public class Journal
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedTime { get; set; }
    }
}
