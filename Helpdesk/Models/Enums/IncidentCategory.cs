﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Helpdesk.Models.Enums
{
    public enum IncidentCategory
    {
        [Display(Name = "Broken Software")]
        BrookenSoftware,
        [Display(Name = "Broken Connection")]
        BrokenConnection,
        [Display(Name = "Server Issues")]
        ServerDown,
        [Display(Name = "Github repository event")]
        GithubEvent
    }
}
