﻿namespace Helpdesk.Models.Enums
{
    public enum ServiceRequestCategory
    {
        Laptops,
        Printers,
        Network,
        Software
    }
}