﻿using System.Data.Entity;
using Helpdesk.Models.Requests;

namespace Helpdesk.Models
{
    public class HelpdeskDbContext :  DbContext
    {
        public HelpdeskDbContext() : base("HelpdeskDbContext")
        {
        }

        public DbSet<Journal> Journals { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<ServiceRequest> ServiceRequests { get; set; }

    }
}