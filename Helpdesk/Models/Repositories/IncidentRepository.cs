﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Migrations;
using Helpdesk.Models.Requests;

namespace Helpdesk.Models.Repositories
{
    public interface IIncidentRepository
    {
        IEnumerable<Incident> GetAll();
        Incident Get(int id);
        Incident Add(Incident incident);
    }

    public class IncidentRepository : IIncidentRepository
    {
        private HelpdeskDbContext _context;

        public IncidentRepository() : this(new HelpdeskDbContext())
        {
        }

        public IncidentRepository(HelpdeskDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Incident> GetAll() => _context.Incidents.ToList();
        public Incident Get(int id) => _context.Incidents.FirstOrDefault(i => i.Id == id);
        public Incident Add(Incident incident)
        {
            var insertedIncident = _context.Incidents.Add(incident);
            _context.SaveChanges();

            return insertedIncident;
        }

        public void Delete(Incident incident)
        {
            var incidentToDelete = Get(incident.Id);

            _context.Incidents.Remove(incidentToDelete);
            _context.SaveChanges();
        }

        public void Update(Incident incident)
        {
            var incidentToUpdate = Get(incident.Id);
            incidentToUpdate.Title = incident.Title;
            incidentToUpdate.Description = incident.Description;
            incidentToUpdate.Category = incident.Category;
            incidentToUpdate.Requestor = incident.Requestor;

            _context.Incidents.AddOrUpdate(incidentToUpdate);
            _context.SaveChanges();
        }
    }
}