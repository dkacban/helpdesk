﻿Feature: Developer

In order to limit my access to resources
As a Developer
I want to see and edit only my own incidents

Background:
	Given I am logged in as user which is in 'Developers' Group
	When I click my username in the top bar
	Then I should see my account details
	And I should see 'My Azure AD Groups' containing only 'Developers'
	When I go main page

Scenario: Displaying only incidents that I've created before
	Then I should only see incidents created by me
	But I shouldn't see incidents created by someone else
	And I shouldn't see incidents created by github web hook

Scenario: Creating new incident
	And I click 'create new incident' button
	Then I should be presented with a form
	When I fill all the data and I submit the form
	Then I should be redirected to incidents list
	And I should see the incident on the list

Scenario: Updating incident
	And I click 'Edit' link in any row
	Then I should be presented with form
	When I change the title of the incident
	And I submit the form
	Then I should be redirected to incidents list
	And I should see updated title there