﻿Feature: Helpdesk

In order to manage all incidents in the system with unlimitted access
As Helpdesk employee
I should be able to see and manipulate all existing incidents

Background:
	Given I am logged in as user which is in 'Helpdesk' Group
	When I click my username in the top bar
	Then I should see my account details
	And I should see 'My Azure AD Groups' containing only 'Helpdesk'
	When I go main page

Scenario: Displaying list of all incidents
	Then I should see incidents created by me
	And I should see incidents created by someone else
	And I should see incidents created by github web hook

Scenario: Creating new incident
	And I click 'create new incident' button
	Then I should be presented with a form
	When I fill all the data and I submit the form
	Then I should be see redorected to incidents list
	And I should see the incident on the list

Scenario: Updating incident
	And I click 'Edit' link in any row
	Then I should be presented with form
	When I change the title of the incident
	And I submit the form
	Then I should be redirected to incidents list
	And I should see updated title there

Scenario: Adding new incident by Github Web Hook
	And I go to https://github.com/dkacban/helpdesk

	Then I should be presented with form
	When I change the title of the incident
	And I submit the form
	Then I should be redirected to incidents list
	And I should see updated title there